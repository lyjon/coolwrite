CFLAGS = -fpermissive
all:hello lc send_serial
hello:track.cpp
	gcc `pkg-config --cflags --libs opencv` $(CFLAGS) -o hello track.cpp
#track.o:track.cpp sound.c 
#	 g++ $(CFLAGS) -c track.cpp sound.c  -lm
#sound.o:sound.c fft.h
#	gcc $(CFLAGS) -c sound.c -lm
	
lc:sound.c fft.h 
	gcc -o lc sound.c fft.h -lm

send_serial:send_serial.c linux_serial.h linux_serial.c
	gcc `pkg-config --cflags --libs opencv`  -o send_serial send_serial.c linux_serial.c -lm

